#!/usr/bin/python

from os import environ
from webserver import app
from config import *

if __name__ == '__main__':
    app.run(FLASK_HOST, FLASK_PORT)
