"""
Routes and views for the flask application.
"""

from datetime import datetime
from flask import render_template, request, url_for
from webserver import app

@app.route('/')
def home():
    """Renders the home page."""
    return render_template(
        'entrance.html',
        title='Is Zi always right?',
        year=datetime.now().year,
    )


@app.route('/disapproval')
def disapproval():
    return render_template('disapproval.html')