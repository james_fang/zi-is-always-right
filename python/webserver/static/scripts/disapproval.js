﻿var g_containerProportion = 0.8;
var g_imageProportion = 0.8;

function resizeImages() {
    var container = document.getElementById("container");
    var mainImage = document.getElementById("mainImage");
    var winWidth = window.innerWidth| document.documentElement.clientWidth || document.body.clientWidth;
    var winHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    var origWidth = mainImage.width;
    var origHeight = mainImage.height;
    var zoomCoeff = Math.min((winWidth / origWidth), (winHeight / origHeight));

    var containerWidth = origWidth * zoomCoeff * g_containerProportion;
    var containerHeight = origHeight * zoomCoeff * g_containerProportion;
    container.style.width = String(containerWidth) + "px";
    container.style.height = String(containerHeight) + "px";
    container.style.left = String((winWidth - containerWidth) / 2) + "px";
    container.style.top = String((winHeight - containerHeight) / 2) + "px";

    var ImageWidth = origWidth * zoomCoeff * g_imageProportion;
    var ImageHeight = origHeight * zoomCoeff * g_imageProportion;
    mainImage.style.width = String(ImageWidth) + "px";
    mainImage.style.height = String(ImageHeight) + "px";
    mainImage.style.left = String((containerWidth - ImageWidth) / 2) + "px";
    mainImage.style.top = String((containerHeight - ImageHeight) / 2) + "px";


}