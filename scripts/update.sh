#!/bin/bash

WEBSERVER_PYTHON="runserver.py"
RSA_PRIV_KEY_FILE="id_rsa"
HOME_DIR="/home/jamesfang888"
GIT_ROOT_PATH="/home/jamesfang888/enviro"
CONFIG_PATH="/home/jamesfang888/config"
DEPLOYMENT_PATH="/var/www/"

# kill existing webserver instance 
echo "Stopping Apache server"
apachectl stop
sleep 5


pushd $GIT_ROOT_PATH

# git pull
ssh-agent bash -c 'ssh-add $HOME_DIR/.ssh/$RSA_PRIV_KEY_FILE; git pull -q'

# webserver configuration
pip install -q -r ./python/requirements.txt

# copy repo to deployment location
cp -f -r $GIT_ROOT_PATH/python $DEPLOYMENT_PATH/
cp -f  $CONFIG_PATH/config.py $DEPLOYMENT_PATH/python

cp -f $GIT_ROOT_PATH/scripts/flask_wsgi.conf /etc/apache2/sites-enabled/

sleep 2

# start webserver
echo "Starting Apache server"
apachectl start

popd
